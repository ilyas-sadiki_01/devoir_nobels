<?php

// Récupérer le nombre de prix nobels dans la base de données pour l'afficher à la place de TO FILL.


require "begin.html";
?>
<h1> List of the nobel prizes </h1>
<?php
require_once "Model.php";
$obj=new Model();
$users=$obj->get_last();
?>
    
    <table border=1>
    <tr>
        <th>id</th>
        <th>Year</th>
        <th>Category</th>
        <th>Name</th>
        <th>birthdate</th>
        <th>birthplace</th>
        <th>county</th>
        <th>motivation</th>
       
    </tr>
    
        <?php foreach ($users as $user) :?>
        <tr>
        <td ><?= $user["id"];?></td>
        <td ><?= $user["year"];?></td>
        <td ><?= $user["category"];?></td>
        <td ><?= $user["name"];?></td>
        <td ><?= $user["birthdate"];?></td>
        <td ><?= $user["birthplace"];?></td>
        <td ><?= $user["county"];?></td>
        <td ><?= $user["motivation"];?></td>
    </tr>
    <?php endforeach ?>

<?php require "end.html"; ?>