<?php
class Model{
        private $pdo;

        public function __construct() {
            $this->pdo = new PDO('mysql:host=localhost;dbname=nobels', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        public function get_last(){
            $sql = "SELECT * FROM nobels ORDER BY year DESC LIMIT 25";
            $result = $this->pdo->query($sql);
            $users = $result->fetchAll(PDO::FETCH_ASSOC);
            return $users;
        }


        public function get_nb_nobel_prizes(){

                $sql = "SELECT count(*) FROM nobels";
                $result = $this->pdo->query($sql);
                $count = $result->fetchColumn();
                return $count;
            }
    

}




?>